@extends($master)
@section('content')
    <div class="exception card my-2">
        <div class="card-header">
            <h3 class="card-title">Site is down</h3>
            <div class="descr">
                There was an error starting up the site. This will often be resolved quickly so please
                try again in a few minutes.
            </div>
        </div>
        <div class="card-body results">
            <h4>Status</h4>
            @if ($results)
                @foreach($results as $name => $result)
                    <div class="text-{{ $result ? 'success' : 'danger' }} mb-2">
                        {{ $name }} is {{ $result ? 'up' : 'DOWN' }}
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@stop
