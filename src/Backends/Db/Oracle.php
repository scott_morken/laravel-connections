<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 8:07 AM
 */

namespace Smorken\Connections\Backends\Db;

use Illuminate\Support\Str;

class Oracle extends Standard
{

    protected function checkCauseIsLostConnectionForThis(\Throwable $e): bool
    {
        $message = $e->getMessage();

        return Str::contains($message, [
            'connection lost',
            'not connected to',
            'communication channel',
        ]);
    }

    protected function doVerifyQuery(): void
    {
        $this->getBackend()
             ->connection($this->name)
             ->select('SELECT 1 FROM "DUAL"');
    }
}
