<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 8:05 AM
 */

namespace Smorken\Connections\Backends\Db;

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\DetectsLostConnections;
use Smorken\Connections\Backends\Base;

class Standard extends Base
{

    use DetectsLostConnections;

    /**
     * @var DatabaseManager
     */
    protected mixed $backend;

    /**
     * @return void
     */
    public function disconnect(): void
    {
        $this->getBackend()->disconnect($this->name);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function verify(): bool
    {
        try {
            $this->doVerifyQuery();
            return true;
        } catch (\Exception $e) {
            if ($this->checkCauseIsLostConnection($e)) {
                $this->getBackend()->reconnect($this->name);
                return true;
            } else {
                throw $e;
            }
        }
    }

    protected function checkCauseIsLostConnection(\Throwable $e): bool
    {
        if ($this->causedByLostConnection($e)) {
            return true;
        }
        return $this->checkCauseIsLostConnectionForThis($e);
    }

    protected function checkCauseIsLostConnectionForThis(\Throwable $e): bool
    {
        return false;
    }

    protected function doVerifyQuery(): void
    {
        $this->getBackend()
             ->connection($this->name)
             ->select('SELECT 1 AS active');
    }
}
