<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 8:04 AM
 */

namespace Smorken\Connections\Backends;

use Smorken\Connections\Contracts\Backend;

abstract class Base implements Backend
{

    protected mixed $backend;

    protected ?string $name;

    public function __construct(mixed $backend, $name = null)
    {
        $this->backend = $backend;
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBackend(): mixed
    {
        return $this->backend;
    }
}
