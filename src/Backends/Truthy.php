<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 8:08 AM
 */

namespace Smorken\Connections\Backends;

class Truthy extends Base
{

    public function __construct()
    {
        parent::__construct(null);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function verify(): bool
    {
        return true;
    }

    /**
     * @return void
     */
    public function disconnect(): void
    {
        return;
    }
}
