<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 7:29 AM
 */

namespace Smorken\Connections\Contracts;

interface Backend
{

    /**
     * @return void
     */
    public function disconnect(): void;

    /**
     * @return mixed
     */
    public function getBackend(): mixed;

    /**
     * @return bool
     * @throws \Exception
     */
    public function verify(): bool;
}
