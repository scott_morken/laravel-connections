<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 7:31 AM
 */

namespace Smorken\Connections\Contracts;

interface Connections
{

    /**
     * @param  null|string  $connection
     * @return void
     */
    public function disconnect(?string $connection = null): void;

    /**
     * @return array
     */
    public function getResults(): array;

    /**
     * @param  null|string  $connection
     * @param  bool  $throw
     * @return bool
     * @throws \Exception
     */
    public function verify(?string $connection = null, bool $throw = true): bool;
}
