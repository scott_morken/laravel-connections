<?php namespace Smorken\Connections\Middleware;

use Closure;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Facades\Response;
use Smorken\Connections\Contracts\Connections;

class Verify
{

    /**
     * @var Repository
     */
    protected Repository $config;

    /**
     * @var Connections
     */
    protected Connections $connections;

    /**
     * Create a new filter instance.
     *
     * @param  Connections  $connections
     * @param  Repository  $config
     */
    public function __construct(Connections $connections, Repository $config)
    {
        $this->connections = $connections;
        $this->config = $config;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->connections->verify(null, false)) {
            $data = $this->connections->getResults();
            if ($request->ajax()) {
                return Response::json($data, 500);
            }
            $master = $this->config->get('connections.master', 'layouts.app');
            $view = $this->config->get('connections.view', 'smorken/connections::default');
            return Response::view($view, ['results' => $data, 'master' => $master]);
        }
        return $next($request);
    }
}
