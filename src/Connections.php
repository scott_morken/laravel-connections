<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 7:33 AM
 */

namespace Smorken\Connections;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Smorken\Connections\Contracts\Backend;

class Connections implements \Smorken\Connections\Contracts\Connections
{

    protected array $backends = [];

    protected ExceptionHandler $exceptionHandler;

    protected array $results = [];

    public function __construct(array $backends, ExceptionHandler $exceptionHandler)
    {
        $this->backends = $backends;
        $this->exceptionHandler = $exceptionHandler;
    }

    public function disconnect(?string $connection = null): void
    {
        if ($connection) {
            $this->disconnectByName($connection);
            return;
        }
        foreach ($this->backends as $name => $backend) {
            $this->disconnectByName($name);
        }
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @param  string|null  $connection
     * @param  bool  $throw
     * @return bool
     * @throws \Exception
     */
    public function verify(?string $connection = null, bool $throw = true): bool
    {
        $this->results = [];
        if ($connection) {
            return $this->verifyByName($connection, $throw);
        }
        $verified = true;
        foreach ($this->backends as $name => $backend) {
            $r = $this->verifyByName($name, $throw);
            if (!$r) {
                $verified = false;
            }
        }
        return $verified;
    }

    protected function disconnectByName(?string $name): void
    {
        $this->getBackend($name)->disconnect();
    }

    /**
     * @param  string  $name
     * @return Backend
     */
    protected function getBackend(string $name): Backend
    {
        $b = array_key_exists($name, $this->backends) ? $this->backends[$name] : null;
        if (is_null($b)) {
            throw new InvalidBackendException("$name is not a valid backend.");
        }
        return $b;
    }

    protected function report(\Throwable $exception): void
    {
        $this->exceptionHandler->report($exception);
    }

    protected function verifyByName(?string $name, bool $throw): bool
    {
        $this->results[$name] = false;
        try {
            $r = $this->getBackend($name)->verify();
            $this->results[$name] = $r;
            return $r;
        } catch (InvalidBackendException $e) {
            throw $e;
        } catch (\Exception $e) {
            if ($throw) {
                throw $e;
            }
            $this->report($e);
            return false;
        }
    }
}
