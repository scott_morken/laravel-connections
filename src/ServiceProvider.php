<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 7:37 AM
 */

namespace Smorken\Connections;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\Arr;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->bootViews();
        $this->bootConfig();
    }

    public function register(): void
    {
        $this->app->bind(
            \Smorken\Connections\Contracts\Connections::class,
            function ($app) {
                $backends = $this->getBackends($app);
                return new Connections($backends, $app[ExceptionHandler::class]);
            }
        );
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'connections');
        $this->publishes([$config => config_path('connections.php')], 'config');
    }

    protected function bootViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'smorken/connections');
        $this->publishes(
            [__DIR__.'/../views' => base_path('resources/views/vendor/smorken/connections'),],
            'views'
        );
    }

    protected function createByType($actual, $type, $app)
    {
        if ($actual) {
            if ($type === 'di') {
                return $app[$actual];
            }
            return new $actual;
        }
        return null;
    }

    protected function getBackends($app): array
    {
        $config = $app['config']->get('connections.backends', []);
        $backends = [];
        foreach ($config as $name => $data) {
            $b = $this->createByType(
                Arr::get($data, 'backend.actual'),
                Arr::get($data, 'backend.type', 'di'),
                $app
            );
            $impl = Arr::get($data, 'impl');
            $conn_name = Arr::get($data, 'connection_name');
            if ($impl && $b) {
                $backends[$name] = new $impl($b, $conn_name);
            }
        }
        return $backends;
    }
}
