<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/16/17
 * Time: 7:19 AM
 */
return [
    'master'   => 'layouts.app',
    'view'     => 'smorken/connections::default',
    'backends' => [
        'Default' => [
            'impl'            => \Smorken\Connections\Backends\Db\Standard::class,
            'backend'         => [
                'type'   => 'di',
                'actual' => 'db',
            ],
            'connection_name' => env('DB_CONNECTION', 'mysql'),
        ],
    ],
];
