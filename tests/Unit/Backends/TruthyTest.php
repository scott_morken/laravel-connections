<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 8:11 AM
 */

namespace Tests\Smorken\Connections\Unit\Backends;

use PHPUnit\Framework\TestCase;
use Smorken\Connections\Backends\Truthy;

class TruthyTest extends TestCase
{

    public function testVerifyIsAlwaysTrue()
    {
        $sut = new Truthy();
        $this->assertTrue($sut->verify());
    }

    public function testDisconnectIsNull()
    {
        $sut = new Truthy();
        $this->assertNull($sut->disconnect());
    }
}
