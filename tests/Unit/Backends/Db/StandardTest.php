<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 8:14 AM
 */

namespace Tests\Smorken\Connections\Unit\Backends\Db;

use Illuminate\Database\DatabaseManager;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Connections\Backends\Db\Standard;

class StandardTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testVerifyWithUnexpectedExceptionThrowsException()
    {
        list($sut, $b) = $this->getSut();
        $e = new \Exception('Foo Exception');
        $b->shouldReceive('connection->select')->once()->andThrow($e);
        $b->shouldReceive('reconnect')->never();
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($e->getMessage());
        $sut->verify();
    }

    public function testVerifyWithExpectedExceptionReconnects()
    {
        list($sut, $b) = $this->getSut();
        $e = new \Exception('server has gone away');
        $b->shouldReceive('connection->select')->once()->andThrow($e);
        $b->shouldReceive('reconnect')->once()->with('db');
        $this->assertTrue($sut->verify());
    }

    public function testDisconnectCallsBackendDisconnect()
    {
        list($sut, $b) = $this->getSut();
        $b->shouldReceive('disconnect')->once()->with('db');
        $this->assertNull($sut->disconnect());
    }

    protected function getSut()
    {
        $b = m::mock(DatabaseManager::class);
        $sut = new Standard($b, 'db');
        return [$sut, $b];
    }
}
