<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 8:25 AM
 */

namespace Tests\Smorken\Connections\Unit;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Connections\Connections;
use Smorken\Connections\Contracts\Backend;
use Smorken\Connections\InvalidBackendException;

class ConnectionsTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testInvalidBackendIsException()
    {
        [$sut, $backends, $logger] = $this->getSut();
        $this->expectException(InvalidBackendException::class);
        $this->expectExceptionMessage('bar is not a valid backend');
        $sut->verify('bar');
    }

    public function testBackendFailsCanRethrowException()
    {
        [$sut, $backends, $logger] = $this->getSut();
        $e = new \Exception('Foo Exception');
        $backends['Default']->shouldReceive('verify')->once()->andThrow($e);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Foo Exception');
        $sut->verify();
    }

    public function testBackendFailsCanReturnFalseInsteadOfException()
    {
        [$sut, $backends, $logger] = $this->getSut();
        $e = new \Exception('Foo Exception');
        $backends['Default']->shouldReceive('verify')->once()->andThrow($e);
        $logger->shouldReceive('report')->once()->with($e);
        $this->assertFalse($sut->verify(null, false));
        $this->assertEquals(['Default' => false], $sut->getResults());
    }

    public function testOneBackendFailsCanReturnFalseInsteadOfException()
    {
        [$sut, $backends, $logger] = $this->getSut(
            [
                'Default' => m::mock(Backend::class),
                'Other'   => m::mock(Backend::class),
            ]
        );
        $e = new \Exception('Foo Exception');
        $backends['Default']->shouldReceive('verify')->once()->andReturn(true);
        $backends['Other']->shouldReceive('verify')->once()->andThrow($e);
        $logger->shouldReceive('report')->once()->with($e);
        $this->assertFalse($sut->verify(null, false));
        $this->assertEquals(['Default' => true, 'Other' => false], $sut->getResults());
    }

    public function testMultipleBackendsFailCanReturnFalseInsteadOfException()
    {
        [$sut, $backends, $logger] = $this->getSut(
            [
                'Default' => m::mock(Backend::class),
                'Other'   => m::mock(Backend::class),
            ]
        );
        $e = new \Exception('Foo Exception');
        $backends['Default']->shouldReceive('verify')->once()->andThrow($e);
        $backends['Other']->shouldReceive('verify')->once()->andThrow($e);
        $logger->shouldReceive('report')->twice()->with($e);
        $this->assertFalse($sut->verify(null, false));
        $this->assertEquals(['Default' => false, 'Other' => false], $sut->getResults());
    }

    public function testOneBackendFailsCanReturnFalse()
    {
        [$sut, $backends, $logger] = $this->getSut(
            [
                'Default' => m::mock(Backend::class),
                'Other'   => m::mock(Backend::class),
            ]
        );
        $backends['Default']->shouldReceive('verify')->once()->andReturn(true);
        $backends['Other']->shouldReceive('verify')->once()->andReturn(false);
        $logger->shouldReceive('report')->never();
        $this->assertFalse($sut->verify());
        $this->assertEquals(['Default' => true, 'Other' => false], $sut->getResults());
    }

    public function testVerifyResetsResults()
    {
        [$sut, $backends, $logger] = $this->getSut(
            [
                'Default' => m::mock(Backend::class),
                'Other'   => m::mock(Backend::class),
            ]
        );
        $backends['Default']->shouldReceive('verify')->once()->andReturn(true);
        $backends['Other']->shouldReceive('verify')->once()->andReturn(false);
        $logger->shouldReceive('report')->never();
        $this->assertFalse($sut->verify());
        $this->assertEquals(['Default' => true, 'Other' => false], $sut->getResults());
        $backends['Other']->shouldReceive('verify')->once()->andReturn(true);
        $this->assertTrue($sut->verify('Other'));
        $this->assertEquals(['Other' => true], $sut->getResults());
    }

    public function testDisconnectCallsBackends()
    {
        [$sut, $backends, $logger] = $this->getSut(
            [
                'Default' => m::mock(Backend::class),
                'Other'   => m::mock(Backend::class),
            ]
        );
        $backends['Default']->shouldReceive('disconnect')->once();
        $backends['Other']->shouldReceive('disconnect')->once();
        $this->assertNull($sut->disconnect());
    }

    protected function getSut($backends = null)
    {
        if (is_null($backends)) {
            $backends = $this->getBackends();
        }
        $logger = m::mock(ExceptionHandler::class);
        $sut = new Connections($backends, $logger);
        return [$sut, $backends, $logger];
    }

    protected function getBackends()
    {
        return [
            'Default' => m::mock(Backend::class),
        ];
    }
}
